## @eyeo/telemetry-javascript SDK


## Configure .npmrc

In order to install the packages, a `.npmrc` file needs to be created in the project root.
Run the following command to create the file:
```bash
echo "
@eyeo:registry=https://gitlab.com/api/v4/packages/npm/
//gitlab.com/api/v4/packages/npm/:_authToken=\"YOUR_AUTH_TOKEN\"
//gitlab.com/api/v4/projects/27103759/packages/npm/:_authToken=\"YOUR_AUTH_TOKEN\"
" >> .npmrc
echo ".npmrc" >> .gitignore
```

Replace `YOUR_AUTH_TOKEN` variable with your personal access token. To create a personal access token follow the
steps from [GitLab Access Tokens](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token)
and select the following scopes:
- api
- read_repository
- write_repository


## Installation
Make sure that you have the `.npmrc` file like above.
```bash
# Using Yarn
yarn add @eyeo/telemetry-browser

# Using NPM
npm i @eyeo/telemetry-browser
```

## How to use

To use this SDK, call the `init` function as early as possible in the main entry module. To set context information or
send manual events, use the provided methods.

```javascript
import * as Telemetry from '@eyeo/telemetry-browser';

Telemetry.init({
  dsn: 'YOUR_DSN',
  schemaVersion: 1,
  // ...
});

Telemetry.sendEvent('page_view', {
  page_path: '/top-10-pizza',
  page_title: 'I love pizza'
})
```

## Contributing
Please see [contributing guide](https://gitlab.com/eyeo/crumbs/telemetry-javascript/-/blob/master/CONTRIBUTING.md).
