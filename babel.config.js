module.exports = function (api) {
  api.cache(() => process.env.NODE_ENV === 'production');

  const presets = [
    '@babel/preset-env',
  ];
  const plugins = [
    ['@babel/plugin-transform-runtime'], // To reuse babel helpers in order to reduce code size
    // ['@babel/plugin-transform-modules-commonjs'],
  ];

  return {
    presets,
    plugins,
    babelrcRoots: [
      '.',
      'packages/*',
    ],
  };
};
