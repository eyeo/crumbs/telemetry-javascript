import { LOG_LEVEL_ERROR, logger } from '@eyeo/telemetry-utils';

import { NoopTransport } from './transports';


let client;

class TelemetryClient {
  constructor() {
    if (client) {
      return client;
    }

    this.__transport = undefined;
    this.__userContext = undefined;
  }

  /**
   *
   * @param {Object} options
   * @param {string} options.dsn - The DSN (Data Source Name) for telemetry reporting.
   * @param {Object} options.app - The application details.
   * @param {string} options.app.name - The name of the application using the SDK.
   * @param {string} options.app.version - The version of the application using the SDK.
   * @param {boolean} [options.debug=false] - when true it'll not do any HTTP request, but rather log it
   * @param {string} [options.logLevel='error'] - Set the log level: info, warn or error
   * @param {string} [options.authorization] - Authorization Bearer to be used for DSN
   * @param {number} [options.sampleRate=1.0] - If set, will send only a percentage of the events into the telemetry system
   */
  setOptions = ({
    dsn,
    authorization,
    app,
    debug = false,
    logLevel = LOG_LEVEL_ERROR,
    sampleRate = 1.0
  }) => {
    // Telemetry system endpoint
    if (!dsn) {
      throw new Error(`Missing required properties: 'dsn'.`);
    }

    if (app && (!app.name || !app.version)) {
      throw new Error(`Missing required properties: 'app.name' or 'app.version'`);
    }

    // If set, will send only a percentage of the events into the telemetry system
    // Min: 0.0 -> 0%
    // Max: 1.0 -> 100%
    if (sampleRate) {
      // eslint-disable-next-line no-restricted-globals
      if (!isFinite(sampleRate) || sampleRate < 0 || sampleRate > 1) {
        throw new Error(`Invalid sample rate value. The sample rate must be between 0.0 and 1.0 (inclusive).`);
      }
    }

    // enable the logger
    logger.setLevel(logLevel);

    this.options = {
      dsn,
      app,
      debug,
      logLevel,
      authorization,
      sampleRate,
    };
    logger.log('Set options', this.options);
  }

  /**
   *
   * @param transportClass
   */
  setTransport = (transportClass) => {
    if (this.options.debug) {
      client.__transport = new NoopTransport(this.options);
      logger.log('Set transport', NoopTransport.name);
      return;
    }

    client.__transport = new transportClass(this.options);
    logger.log('Set transport', transportClass.name);
  };


  /**
   * Set the user context which can contain:
   * - userId: The ID of the user. Can be fleeting or permanent. (Optional)
   * - userEmail: The email of the user. (Optional)
   * - others
   *
   * @param userContext
   */
  setUserContext = (userContext) => {
    this.__userContext = userContext;
    logger.log('setUserContext:', this.__userContext);
  };

  /**
   *
   * @param eventName The name of the event.
   *    e.g. page_view
   * @param eventParams Object containing the event parameters
   *    e.g. { page_path: '/top-10-pizza', page_title: 'I love pizza'}
   */
  sendEvent = async (eventName, eventParams) => {
    const telemetryPayload = {
      eventName,
      eventParams,
      user: this.__userContext,
      createdAt: new Date().toISOString(),
    };

    return this.__transport.sendEvent(telemetryPayload);
  }


  /**
   *
   * @param eventName
   * @param eventParams
   */
  enqueueEvent = (eventName, eventParams) => {
    throw new Error('Enqueue event is not implemented.');
  }
}

client = new TelemetryClient();

export const setOptions = client.setOptions;
export const setTransport = client.setTransport;
export const setUserContext = client.setUserContext;
export const sendEvent = client.sendEvent;
export const enqueueEvent = client.enqueueEvent;

export { BaseTransport } from './transports';
