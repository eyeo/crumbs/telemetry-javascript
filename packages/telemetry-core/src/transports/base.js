
/**
 * Base Transport class implementation
 */
export class BaseTransport {
  /**
   *
   * @param options
   */
  constructor(options) {
    this._url = options.dsn;
    this._app = options.app;
    this._authorization = options.authorization;

    // A simple buffer holding all requests.
    // this._buffer = []; // PromiseBuffer(30);
  }


  /**
   * Transforms an event name and payload based on the Telemetry Schema
   *
   * @param telemetryPayload
   * @returns {{schemaVersion, payload}}
   * @private
   */
  _eventToTelemetryFormat = (telemetryPayload) => {
    return {
      payload:  {
        app: this._app,
        ...telemetryPayload
      },
    }
  };


  /**
   * Sends the event into the Telemetry system.
   *
   * @param telemetryPayload Event that should be sent to Telemetry.
   */
  async sendEvent(telemetryPayload) {
    throw new Error('Transport Class has to implement `sendEvent` method');
  }


  /**
   * Enqueue the event to the storage and prepare it to be sent into the Telemetry system.
   *
   * @param telemetryPayload Event that should be sent to Telemetry.
   */
  enqueueEvent(telemetryPayload) {
    throw new Error('Transport Class has to implement `enqueueEvent` method');
  }
}
