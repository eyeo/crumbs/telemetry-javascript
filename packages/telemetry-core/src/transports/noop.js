import { logger } from '@eyeo/telemetry-utils/src/logger';
import { BaseTransport } from './base';

export class NoopTransport extends BaseTransport {
  /**
   *
   * @param telemetryPayload
   */
  sendEvent(telemetryPayload) {
    logger.log('sendEvent:', this._eventToTelemetryFormat(telemetryPayload));
  }
}
