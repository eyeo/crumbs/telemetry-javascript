import { BaseTransport } from '@eyeo/telemetry-core';
import { getGlobalObject, logger } from '@eyeo/telemetry-utils';


/**
 * `fetch` based transport
 */
export class FetchTransport extends BaseTransport {
  constructor(options) {
    super(options);

    const global = getGlobalObject();
    this._fetch = global.fetch.bind(global);
  }


  /**
   * @inheritDoc
   */
  async sendEvent(telemetryPayload) {
    return this._sendRequest(this._eventToTelemetryFormat(telemetryPayload), telemetryPayload);
  }


  /**
   * @param telemetryPayload Prepared telemetry payload to be delivered, contains schema version
   * @param originalPayload
   * @returns {Promise<void>}
   * @private
   */
  _sendRequest = async (telemetryPayload, originalPayload) => {
    // TODO (alexm): check rate limit
    // if (isRateLimited(telemetryPayload)) {
    //   return Promise.reject({
    //     event: originalPayload.eventName,
    //     reason: `Rate limited due to too many requests.`,
    //     status: 429,
    //   });
    // }

    const options = {
      body: JSON.stringify(telemetryPayload),
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      }
    };

    if (this._authorization) {
      options.headers['Authorization'] = `Bearer ${this._authorization}`;
    }

    try {
      logger.log('Fetch request', this._url, options);
      const req = await this._fetch(this._url, options);

      if (!req.ok) {
        logger.error(`Fetch error occurred while sending the event: ${originalPayload.eventName}, http_status=${req.status}`);
        return false;
      }

      logger.log('Fetch response:', req);
      return true;
    }
    catch (err) {
      logger.error(`An error occurred while sending the event: ${originalPayload.eventName}`, err);
      return false;
    }
  }
}
