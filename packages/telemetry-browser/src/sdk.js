/* eslint-disable no-param-reassign */

import { setOptions, setTransport } from '@eyeo/telemetry-core';
import { FetchTransport } from './transports';

/**
 * The Telemetry SDK Client.
 *
 * To use this SDK, call the {@link init} function as early as possible in the
 * main entry module. To set context information or send manual events, use the
 * provided methods.
 *
 * @example
 * ```
 * import * as Telemetry from '@eyeo-telemetry/browser';
 *
 * Telemetry.init({
 *   dsn: 'YOUR_DSN',
 *   // ...
 * });
 * ```
 *
 * @param options
 */
export const init = (options = {}) => {
  setOptions(options);
  setTransport(FetchTransport);
};
