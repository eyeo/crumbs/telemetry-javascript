import { isNodeEnv } from './node';

const fallbackGlobalObject = {};

/**
 * Safely get global scope object
 *
 * @returns Global scope object
 */
export function getGlobalObject() {
  return (isNodeEnv()
    ? global
    : typeof window !== 'undefined'
      ? window
      : typeof self !== 'undefined'
        ? self
        : fallbackGlobalObject);
}
