/**
 * Tells whether current environment supports Fetch API
 * {@link supportsFetch}.
 *
 * @returns Boolean
 */
export function supportsFetch() {
  return 'fetch' in global;
}
