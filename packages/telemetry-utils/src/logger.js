import { getGlobalObject } from './misc';

const PREFIX = '[telemetry]';
const global = getGlobalObject();

const LOG_LEVEL_INFO = 'info';
const LOG_LEVEL_WARN = 'warn';
const LOG_LEVEL_ERROR = 'error';
const LEVELS = [LOG_LEVEL_INFO, LOG_LEVEL_WARN, LOG_LEVEL_ERROR];

let logger;

class Logger {
  constructor() {
    // Ensure we only have a single logger instance
    if (logger) {
      return logger;
    }

    this._level = LOG_LEVEL_ERROR;
  }

  setLevel(level) {
    if (LEVELS.indexOf(level.toLowerCase()) < 0) {
      global.console.log(`${PREFIX} Invalid verbose level`, level);
      return;
    }

    this._level = level.toLowerCase();
  }

  log(...args) {
    if (LEVELS.indexOf(this._level) > 0) {
      return;
    }

    global.console.log(`${PREFIX}[${LOG_LEVEL_INFO}]`, ...args);
  }

  warn(...args) {
    if (LEVELS.indexOf(this._level) > 1) {
      return;
    }

    global.console.warn(`\x1b[33m${PREFIX}[${LOG_LEVEL_WARN}]`, ...args, '\x1b[0m');
  }

  error(...args) {
    global.console.error(`\x1b[31m${PREFIX}[${LOG_LEVEL_ERROR}]`, ...args, '\x1b[0m');
  }
}

logger = new Logger();

export {
  logger,
  LOG_LEVEL_INFO,
  LOG_LEVEL_WARN,
  LOG_LEVEL_ERROR,
};
