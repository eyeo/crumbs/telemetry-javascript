#!/bin/bash

## Cleans dependency folders
rm -Rf node_modules
rm -rf packages/*/node_modules

## Cleans dist folders
rm -rf packages/*/dist
