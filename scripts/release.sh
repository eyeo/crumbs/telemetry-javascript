#!/bin/bash

# ******************************************************************************************
# This releases an update to the `@eyeo/telemetry-javascript` package.
# Don't use `npm publish` for it.
# Read the release instructions:
# https://gitlab.com/eyeo/crumbs/telemetry-javascript/-/blob/master/CONTRIBUTING.md#how-to-do-a-release
# ******************************************************************************************

# Exit the script on any command with non 0 return code
# We assume that all the commands in the pipeline set their return code
# properly and that we do not need to validate that the output is correct
set -e

# Echo every command being executed
# set -x

if [ -n "$(git status --porcelain)" ]; then
  printf "\n\n\033[0;31mYour git status is not clean. Aborting.\033[0m\n";
  printf "\033[0;31mPlease make sure you have committed and pushed all your changes first.\033[0m\n\n\n";
  exit 1;
fi

## Cleans dependency folders
yarn clean

## Install node modules
yarn install

## Increment version
# --force-publish - force publish all packages, this will skip the lerna changed check for changed packages and forces a package that didn't have a git diff change to be updated.
lerna version --force-publish

## Build with babel the sources
yarn build

## Publish the modules into the registry
# from-git        - explicitly publish packages tagged in the current commit
# --contents dist - will pack and upload the contents of the dist folder as the source for the package.
# --loglevel      - silly for debug purposes
# --yes           - skip all confirmation prompts
lerna publish from-git --contents dist
