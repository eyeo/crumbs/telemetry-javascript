import * as Telemetry from '@eyeo/telemetry-browser';

global.fetch = require('node-fetch');

Telemetry.init({
  dsn: 'https://httpstat.us/201',
  schemaVersion: 1,
  // debug: true,
  logLevel: Telemetry.LOG_LEVEL_INFO,
});


async function run() {
  console.log('-----------------------> Before all page_view');

  await Telemetry.sendEvent('page_view', {
    page_path: '/top-10-pizza',
    page_title: 'I love pizza'
  });

  console.log('-----------------------> After first page_view');

  Telemetry.sendEvent('page_view', {
    page_path: '/landing',
    page_title: 'I love landings'
  });

  console.log('-----------------------> After second page_view');
}

run();
