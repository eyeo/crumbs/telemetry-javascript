## Contributing to @eyeo/telemetry-javascript SDK

Please take a moment to review this document in order to make the contribution process straightforward and effective for
everyone involved.

Before starting to change something, please **ask first** if somebody else is already working on this, or the core
developers think your feature is in-scope for the SDK.

## How to do a release

1. Make sure you have publish access for all packages:
    - You must be in the gitlab @eyeo organization
    - You must have publish access to the telemetry-javascript repository

2. Run `yarn release`, follow prompts

3. Verify
    1. If everything works properly, the tag should have been auto-pushed. Go to 4.
    2. If the publish fails half-way, things have gotten hairy. Now you need to go
       to [GitLab Package Registry](https://gitlab.com/eyeo/crumbs/telemetry-javascript/-/packages) to check which
       packages have been published and manually publish the ones that have not been published yet. To restart the
       process you need to remove the auto-pushed tags, the already published artifacts and the latest commit. 
       Here are some commands that might help you:
       ```shell
       # Delete tag from local and remote 
       git tag -d v2.0.0 && git push --delete origin v2.0.0
       
       # Remove the latest commit (should contain the version bump)
       git reset --hard HEAD^
       
       # Remove the latest commit from the remote branch
       git push origin +HEAD
       ```

4. Go to [GitLab Package Registry](https://gitlab.com/eyeo/crumbs/telemetry-javascript/-/packages) page and verify that
   the released version is present
